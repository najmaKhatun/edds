package com.edds.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.edds.entity.Contact;
import com.edds.entity.MyAPIResponseTO;
import com.edds.service.IContactService;


@RestController
@RequestMapping("/api/eddsV1/")
public class ContactController {
	   @Autowired
	   private IContactService contactService;
	   @GetMapping("contact/{id}")
		public MyAPIResponseTO getContactById(@PathVariable("id") Integer id) {
		    Contact contact = contactService.getContactById(id);
			return new MyAPIResponseTO("Contact fetched successfully",1,contact);
		}
	  
		@GetMapping("contacts")
		public MyAPIResponseTO getAllContacts() {
			List<Contact> list = contactService.getAllContacts();
			return new MyAPIResponseTO("Contacts fetched successfully",1,list);
		}
		
		@PostMapping("add_contact")
		public MyAPIResponseTO addContact(@RequestBody Contact contact) {
		if((contact.getTitle() != "" && contact.getTitle() != null) && 
		   (contact.getFirst_name() != "" && contact.getFirst_name() != null) &&
		   (contact.getMiddle_name() != "" && contact.getMiddle_name() != null) && 
		   (contact.getLast_name() != "" && contact.getLast_name() != null)	) {
		      boolean flag = contactService.addContact(contact);
	            if (flag == false) {
	            	return new MyAPIResponseTO("Contact already exists",0);
	            }
	            else {
	            	return new MyAPIResponseTO("Contact added successfully",1);
	            }
				}
				else {
	            	return new MyAPIResponseTO("Please enter all the mandatory fields",1);	
				}
		}
		
		@PostMapping("update_contact")
		public MyAPIResponseTO updateContact(@RequestBody Contact contact) {
			if((contact.getTitle() != "" && contact.getTitle() != null) && 
					   (contact.getFirst_name() != "" && contact.getFirst_name() != null) &&
					   (contact.getMiddle_name() != "" && contact.getMiddle_name() != null) && 
					   (contact.getLast_name() != "" && contact.getLast_name() != null)	) {
				contactService.updateContact(contact);
				return new MyAPIResponseTO("Contacts updated successfully",1);
			}
			else {
            	return new MyAPIResponseTO("Please enter all the mandatory fields",1);	
			}
		}
		
		@PostMapping("delete_contact/{id}")
		public MyAPIResponseTO<Void> deleteContact(@PathVariable("id") Integer id) {
			contactService.deleteContact(id);
        	return new MyAPIResponseTO("Contact deleted successfully",1);
		}
		
		 @GetMapping("search_contact")
			public MyAPIResponseTO searchContactList(
					@RequestParam("company_name") String company_name,
					@RequestParam("company_website") String company_website,
					@RequestParam("sector_of_operation") String sector_of_operation,
					@RequestParam("designation") String designation,
					@RequestParam("department") String department) {
			    List<Contact> contact = contactService.searchContacts(company_name, company_website, 
			    		sector_of_operation, designation, department);
				return new MyAPIResponseTO("Contact Search Result Fetched Successfully",1,contact);
			}
		
}
