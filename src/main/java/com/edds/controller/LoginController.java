package com.edds.controller;

import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.edds.commonUtil.CommonUtility;
import com.edds.dao.AuthenticationDAO;
import com.edds.entity.LoginCredentials;
import com.edds.entity.MyAPIResponseTO;
import com.edds.entity.UserToken;
import com.edds.exception.MyException;
import com.edds.service.ILoginService;

@RestController
@RequestMapping("/api/eddsV1/")

public class LoginController {
	  @Autowired
	  private ILoginService loginService;
	  @SuppressWarnings({ "rawtypes", "unchecked" })
	  @RequestMapping(value="/login",method=RequestMethod.POST)
	  public MyAPIResponseTO loginUser(@RequestBody LoginCredentials user) throws SQLException,MyException{  
		  try {
			  	UserToken userToken = new UserToken();
			  	LoginCredentials loginCred = new LoginCredentials();

				String password = user.getPassword();
				String loginID = user.getLoginid();
				if ( password == null || loginID == null || password.length() == 0 || loginID.length() == 0 ) {
					return new MyAPIResponseTO("Invalid User Name Or Password",0);
				}
				
				AuthenticationDAO authUser = new AuthenticationDAO();				
				String pwCompareStr = loginService.fetchUserPwd(loginID);
		
				boolean valid = authUser.authenticate(password, pwCompareStr);
				
				if(valid) {
					String userid = Integer.toString(loginService.fetchUserId(loginID));
					loginCred = loginService.fetchUserDetails(userid);
					
					LoginCredentials data = new LoginCredentials();
					boolean tokenExists = loginService.checkTokenExists(userid);
					//checks user already in token database and validate token
					if(tokenExists == false) {
						if(loginCred != null) {						
							userToken.setUserid(Integer.parseInt(loginCred.getUserid()));
							userToken.setFrom_date(loginCred.getActive_from());
							userToken.setTo_date(loginCred.getActive_upto());
							userToken.setToken(CommonUtility.issueToken());
							loginService.saveUserToken(userToken);
							loginCred.setToken(userToken.getToken());
							data = loginCred;
							return new MyAPIResponseTO("Login Successful",1,data);
						}
						else {	
							return new MyAPIResponseTO("No User Details Found",0);
						}
					}
					 else {
						 	userToken.setToken(CommonUtility.issueToken());
						 	userToken.setUserid(Integer.parseInt(loginCred.getUserid()));
							userToken.setFrom_date(loginCred.getActive_from());
							userToken.setTo_date(loginCred.getActive_upto());
						 	loginService.updateUserToken(userToken);
						 	loginCred.setToken(userToken.getToken());
							data = loginCred;
							return new MyAPIResponseTO("Login Successful",1,data);
						}
					
				}
				else {
					return new MyAPIResponseTO("Invalid User Name Or Password",0);
				}
		  }
		  catch (Exception ex) {
				ex.printStackTrace();
				return new MyAPIResponseTO("No login Credentials",0);
			}
	    } 
	  
	  @RequestMapping(value="/logout",method=RequestMethod.POST)
	  public MyAPIResponseTO logoutUser(@RequestHeader("Authorization")String authorizationHeader) throws SQLException,MyException{
		  String token = authorizationHeader.substring("Bearer".length()).trim();
		  int userid = loginService.fetchUserIdByToken(token);
		  loginService.removeUserToken(userid);	  
		  return new MyAPIResponseTO("Logged out successfully",1);
	  	}
	  }
	  

