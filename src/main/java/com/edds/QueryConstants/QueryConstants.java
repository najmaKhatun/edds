package com.edds.QueryConstants;

public class QueryConstants {
	public static String QUERY_INSERT_USER = "INSERT INTO edds_user (userid,user_fnm,user_mail,user_phone,pwd,active_from,"
			+ "active_upto,loginid,last_login,isactive,user_lnm,created_on,created_by,updated_on,updated_by) VALUES "
			+ "(1, 'Najma','najma.khatun@techl33t.in','9875536628','1234','2018-01-01','2019-01-01','nkhatun',"
			+ "'2017-12-05 10:30 AM',1,'Khatun','2018-02-02',1,'2018-02-02',1)";
	public static String QUERY_INSERT_TOKEN = "insert into user_token (token_id, login_id, from_date, "
			+ "to_date, token) values (?, ?, (SELECT CURRENT_DATE), (SELECT CURRENT_DATE + INTERVAL '30 day'), ?)";
	public static String QUERY_FETCH_PWD_BY_LOGIN = "select pwd from edds_user where loginid = ?";
	public static String QUERY_FETCH_USERID_BY_LOGIN = "select userid from edds_user where loginid = ?";
	public static String QUERY_FETCH_TOKEN_BY_USERID = "select token from user_token where userid = ?";
	public static String QUERY_FETCH_USER_BY_LOGIN = "select * from edds_user where loginid =  ?";
	public static String QUERY_GET_USERID_FROM_TOKEN = "select userid from user_token where token = ?";
	// Query For Contact
	public static String QUERY_FETCH_ALL_CONTACTS = "select * from contact order by contact_id";
	public static String QUERY_CHECK_CONTACT_EXISTS = "select * from contact WHERE title = ? "
			+ "and first_name =? and middle_name = ? and last_name = ?";
	public static String QUERY_SEARCH_CONTACTS = "SELECT * FROM contact WHERE  "
			+ "LOCATE(LOWER(?),LOWER(company_name)) > 0 "
			+ "and LOCATE(LOWER(?),LOWER(company_website)) > 0 and "
			+ "LOCATE(LOWER(?),LOWER(sector_of_operation)) > 0 and "
			+ "LOCATE(LOWER(?),LOWER(designation)) > 0 and "
			+ "LOCATE(LOWER(?),LOWER(department)) > 0";

}
