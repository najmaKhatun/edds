package com.edds.commonUtil;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.Random;

import sun.misc.BASE64Encoder;

public class CommonUtility {
	   public static String encryptSHA1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException, NoSuchProviderException{
	    	String pwCompareStr = "";
	    	byte[] messageByte = text.getBytes();
	    	
	        MessageDigest sha1 = MessageDigest.getInstance("SHA-1", "SUN");
	        sha1.update(messageByte);
	        
	        byte[] digestByte = sha1.digest();
	        BASE64Encoder b64Encoder = new BASE64Encoder();
	        pwCompareStr = (b64Encoder.encode(digestByte));
	        pwCompareStr = new StringBuilder("{SHA-1}").append(pwCompareStr).toString();
	        return pwCompareStr;
	    }
//	   Generate Token
	   public static String issueToken() {
			Random random = new SecureRandom();
			String token = new BigInteger(159, random).toString(32);
			return token;
		}
}
