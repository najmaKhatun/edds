package com.edds.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edds.dao.AuthenticationDAO;
import com.edds.dao.ILoginDAO;
import com.edds.entity.LoginCredentials;
import com.edds.entity.UserToken;

@Service
public class LoginService implements ILoginService{
	@Autowired
	private ILoginDAO loginDAO;
	@Autowired
	private AuthenticationDAO authDAO;

	@Override
	public String fetchUserPwd(String loginId) {
		return authDAO.fetchUserPwd(loginId);
	}

	@Override
	public boolean checkTokenExists(String loginId) {
		return loginDAO.checkTokenExists(loginId);
	}

	@Override
	public LoginCredentials fetchUserDetails(String userid) {
		// TODO Auto-generated method stub
		return loginDAO.fetchUserDetails(userid);
	}

	@Override
	public void saveUserToken(UserToken userToken) {
		// TODO Auto-generated method stub
		loginDAO.saveUserToken(userToken);
	}

	@Override
	public int fetchUserId(String loginId) {
		// TODO Auto-generated method stub
		return loginDAO.fetchUserId(loginId);
	}

	@Override
	public void updateUserToken(UserToken userToken) {
		// TODO Auto-generated method stub
		loginDAO.updateUserToken(userToken);
	}

	@Override
	public void removeUserToken(int userid) {
		// TODO Auto-generated method stub
		loginDAO.deleteUserToken(userid);
	}

	@Override
	public int fetchUserIdByToken(String token) {
		// TODO Auto-generated method stub
		return loginDAO.fetchUserIdByToken(token);
	}
	
	
}
