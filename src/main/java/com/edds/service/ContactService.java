package com.edds.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edds.dao.IContactDAO;
import com.edds.entity.Contact;

@Service
public class ContactService implements IContactService{
	@Autowired
	private IContactDAO contactDAO;
	@Override
	public List<Contact> getAllContacts() {
		// TODO Auto-generated method stub
		return contactDAO.getAllContacts();
	}

	@Override
	public Contact getContactById(int contactId) {
		// TODO Auto-generated method stub
		Contact obj = contactDAO.getContactById(contactId);
		return obj;
	}

	@Override
	public boolean addContact(Contact contact) {
		// TODO Auto-generated method stub
		boolean exists = contactDAO.contactExists(contact.getTitle(), 
				contact.getFirst_name(), contact.getMiddle_name(), contact.getLast_name());
		System.out.println("exists"+exists);
		if(exists == true) 
		{
			return false;
		}
		else {
			contactDAO.addContact(contact);
			return true;
		}
	}

	@Override
	public void updateContact(Contact contact) {
		// TODO Auto-generated method stub
		contactDAO.updateContact(contact);

	}

	@Override
	public void deleteContact(int contactId) {
		// TODO Auto-generated method stub
		contactDAO.deleteContact(contactId);
	}

	@Override
	public List<Contact> searchContacts(String company_name, String company_website, String sector_of_operation,
			String designation, String department) {
		// TODO Auto-generated method stub
		return contactDAO.searchContacts(company_name, company_website, sector_of_operation, designation, department);
	}

}
