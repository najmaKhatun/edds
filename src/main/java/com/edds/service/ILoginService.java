package com.edds.service;

import java.util.List;
import com.edds.entity.LoginCredentials;
import com.edds.entity.UserToken;

public interface ILoginService {
    String fetchUserPwd(String loginId);
    boolean checkTokenExists(String loginId);
    LoginCredentials fetchUserDetails(String userid);
    void saveUserToken(UserToken userToken);
    int fetchUserId(String loginId);
	void updateUserToken(UserToken userToken);
    void removeUserToken(int userid);
	int fetchUserIdByToken(String token);
}
