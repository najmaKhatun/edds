package com.edds.service;

import java.util.List;

import com.edds.entity.Contact;

public interface IContactService {
	 List<Contact> getAllContacts();
     Contact getContactById(int contactId);
     boolean addContact(Contact contact);
     void updateContact(Contact contact);
     void deleteContact(int contactId);
     List<Contact> searchContacts(String company_name, String company_website,
 			String sector_of_operation,String designation,String department);
}
