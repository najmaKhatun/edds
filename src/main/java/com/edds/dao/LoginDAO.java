package com.edds.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import com.edds.QueryConstants.QueryConstants;
import com.edds.entity.LoginCredentials;
import com.edds.entity.UserToken;

@Transactional
@Repository
public class LoginDAO implements ILoginDAO {
	@PersistenceContext	
	private EntityManager entityManager;
	@Override
	public boolean checkTokenExists(String loginid) {
		// TODO Auto-generated method stub
		String fetchTokenQuery = QueryConstants.QUERY_FETCH_TOKEN_BY_USERID;
		int count = entityManager.createNativeQuery(fetchTokenQuery).setParameter(1, loginid)
	              .getResultList().size();
		return count > 0 ? true : false;
	}
	
	@Override
	public LoginCredentials fetchUserDetails(String userid) {
		System.out.println("userid"+userid);
		// TODO Auto-generated method stub
		LoginCredentials li = new LoginCredentials();
		try {
			 li = entityManager.find(LoginCredentials.class, userid);	
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return li;
	}

	
	@Override
	public void saveUserToken(UserToken userToken) {
		// TODO Auto-generated method stub
		entityManager.persist(userToken);
		
	}
	
	@Override
    public int fetchUserId(String loginId) {
		String fetchIdQuery = QueryConstants.QUERY_FETCH_USERID_BY_LOGIN;
		return (int) entityManager.createNativeQuery(fetchIdQuery).setParameter(1, loginId).getSingleResult();
    }

	@Override
	public void updateUserToken(UserToken userToken) {
		UserToken userTokenDetails = getUserTokenDetailsByUserId(userToken.getUserid());
		userTokenDetails.setToken(userToken.getToken());
		userTokenDetails.setFrom_date(userToken.getFrom_date());
		userTokenDetails.setTo_date(userToken.getTo_date());
		entityManager.flush();
	}
	
	@Override
	public UserToken getUserTokenDetailsByUserId(int userid) {
		// TODO Auto-generated method stub
		return entityManager.find(UserToken.class,userid);
	}
	
	@Override
	public UserToken getUserTokenById(int userid) {
		// TODO Auto-generated method stub
		return entityManager.find(UserToken.class,userid);
	}
	
	@Override
	public void deleteUserToken(int userid) {
		// TODO Auto-generated method stub
		entityManager.remove(getUserTokenById(userid));
		
	}
	@Override
    public int fetchUserIdByToken(String token) {
		String fetchIdTokenQuery = QueryConstants.QUERY_GET_USERID_FROM_TOKEN;
		int userid = 0;
		try {
			  userid = (int) entityManager.createNativeQuery(fetchIdTokenQuery).setParameter(1, token).getSingleResult();		
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return userid;
    }
}
