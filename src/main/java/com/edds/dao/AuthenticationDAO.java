package com.edds.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import com.edds.QueryConstants.QueryConstants;

@Transactional
@Repository
public class AuthenticationDAO {
	@PersistenceContext	
	private EntityManager entityManager;
    public String fetchUserPwd(String loginId) {
		String fetchPwdQuery = QueryConstants.QUERY_FETCH_PWD_BY_LOGIN;
		return   (String) entityManager.createNativeQuery(fetchPwdQuery).setParameter(1, loginId).getSingleResult();
    };
    
	public boolean authenticate(String hashedPass, String pwCompareStr) {
		if(hashedPass.equals(pwCompareStr))
			return true;
		else 
			return false;
	}
	
	public boolean checkTokenValidity(String token) {
		String fetchUseridQuery = QueryConstants.QUERY_GET_USERID_FROM_TOKEN;
		 int count = entityManager.createNativeQuery(fetchUseridQuery).setParameter(1, token)
	              .getResultList().size();
		return count > 0 ? true : false;
	}

}
