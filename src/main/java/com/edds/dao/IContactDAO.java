package com.edds.dao;

import java.util.List;

import com.edds.entity.Contact;

public interface IContactDAO {
	List<Contact> getAllContacts();
	Contact getContactById(int contactid);
	void addContact(Contact contact);
	void updateContact(Contact contact);
	void deleteContact(int contactid);
    boolean contactExists(String title, String first_name,String middle_name,String last_name);
	List<Contact> searchContacts(String company_name, String company_website,
			String sector_of_operation,String designation,String department);
}
