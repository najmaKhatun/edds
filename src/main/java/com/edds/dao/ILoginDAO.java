package com.edds.dao;

import com.edds.entity.LoginCredentials;
import com.edds.entity.UserToken;

public interface ILoginDAO {
    boolean checkTokenExists(String loginid);
    LoginCredentials fetchUserDetails(String userid);
    void  saveUserToken(UserToken userToken);
    int fetchUserId(String loginid);
    UserToken getUserTokenById(int userid);
	void updateUserToken(UserToken userToken);
	void deleteUserToken(int userid);
	int fetchUserIdByToken(String token);
	UserToken getUserTokenDetailsByUserId(int userid);
}
