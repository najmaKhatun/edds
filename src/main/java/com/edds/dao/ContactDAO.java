package com.edds.dao;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import com.edds.QueryConstants.QueryConstants;
import com.edds.entity.Contact;

@Transactional
@Repository
public class ContactDAO implements IContactDAO{
	@PersistenceContext	
	private EntityManager entityManager;
	@SuppressWarnings("unchecked")
	@Override
	public List<Contact> getAllContacts() {
		// TODO Auto-generated method stub
		List<Contact> list = new ArrayList<Contact>();

		String fetchContactQuery = QueryConstants.QUERY_FETCH_ALL_CONTACTS;
		List<Object[]> li =  (List<Object[]>) entityManager.createNativeQuery(fetchContactQuery).getResultList();
		for(Object[] ob : li){
		    Contact contact = new Contact();
			for(int i=0;i<ob.length;i++) {
				if(i== 0) {
				  contact.setContact_id(Integer.parseInt(ob[i].toString()));
				}
				if(i== 1) {
					contact.setTitle(ob[1].toString());
					}
				if(i== 2) {
					contact.setFirst_name(ob[2].toString());
					}
				if(i== 3) {
					contact.setMiddle_name(ob[3].toString());
					}
				if(i== 4) {
					contact.setLast_name(ob[4].toString());
					}
				if(i== 5) {
					contact.setCompany_name(ob[5].toString());
					}
				if(i== 6) {
					contact.setRole(ob[6].toString());
					}
				if(i== 7) {
					contact.setDesignation(ob[7].toString());
					}
				if(i== 8) {
					contact.setDepartment(ob[8].toString());
					}
				if(i== 9) {
					contact.setSector_of_operation(ob[9].toString());
					}
				if(i== 10) {
					contact.setCompany_type(ob[10].toString());
					}
				if(i== 11) {
					contact.setOfficial_emailid(ob[11].toString());
					}
				if(i== 12) {
					contact.setPersonal_emailid(ob[12].toString());
					}
				if(i== 13) {
					contact.setContactno_mobile(ob[13].toString());
					}
				if(i== 14) {
					contact.setContactno_office(ob[14].toString());
					}
				if(i== 15) {
					contact.setContactno_home(ob[15].toString());
					}
				if(i== 16) {
					contact.setFax1(ob[16].toString());
					}
				if(i== 17) {
					contact.setFax2(ob[17].toString());
					}
				if(i== 18) {
					contact.setCompany_website(ob[18].toString());
					}
				if(i== 19) {
					contact.setKpmg_emailid(ob[19].toString());
					}
				if(i== 20) {
					contact.setLevel_of_relationship(ob[20].toString());
					}
				if(i== 21) {
					contact.setAddress(ob[21].toString());
					}
				if(i== 22) {
					contact.setPermanent_address(ob[22].toString());
					}
				if(i== 23) {
					contact.setCity(ob[23].toString());
					}
				if(i== 24) {
					contact.setZip(ob[24].toString());
					}
				if(i== 25) {
					contact.setCountry(ob[25].toString());
					}
				if(i== 26) {
					contact.setContact_info_last(ob[26].toString());					}
				}
		  list.add(contact);
		}
	  return list;
	}

	@Override
	public void addContact(Contact contact) {
		// TODO Auto-generated method stub
		entityManager.persist(contact);	
	}

	@Override
	public void updateContact(Contact contact) {
		// TODO Auto-generated method stub
		Contact cntct = getContactById(contact.getContact_id());
		// set updated value
		cntct.setTitle(contact.getTitle());
		cntct.setFirst_name(contact.getFirst_name());
		cntct.setMiddle_name(contact.getMiddle_name());
		cntct.setLast_name(contact.getLast_name());
		cntct.setCompany_name(contact.getCompany_name());
		cntct.setRole(contact.getRole());
		cntct.setDesignation(contact.getDesignation());
		cntct.setDepartment(contact.getDepartment());
		cntct.setSector_of_operation(contact.getSector_of_operation());
		cntct.setCompany_type(contact.getCompany_type());
		cntct.setOfficial_emailid(contact.getOfficial_emailid());
		cntct.setPersonal_emailid(contact.getPersonal_emailid());
		cntct.setContactno_mobile(contact.getContactno_mobile());
		cntct.setContactno_office(contact.getContactno_office());
		cntct.setContactno_home(contact.getContactno_home());
		cntct.setFax1(contact.getFax1());
		cntct.setFax2(contact.getFax2());
		cntct.setCompany_website(contact.getCompany_website());
		cntct.setKpmg_emailid(contact.getKpmg_emailid());
		cntct.setLevel_of_relationship(contact.getLevel_of_relationship());
		cntct.setAddress(contact.getAddress());
		cntct.setPermanent_address(contact.getPermanent_address());
		cntct.setCity(contact.getCity());
		cntct.setZip(contact.getZip());
		cntct.setCountry(contact.getCountry());
		cntct.setContact_info_last(contact.getContact_info_last());
		entityManager.flush();
		
	}

	@Override
	public void deleteContact(int contactid) {
		// TODO Auto-generated method stub
		entityManager.remove(getContactById(contactid));
	}

	@Override
	public Contact getContactById(int contactid) {
		// TODO Auto-generated method stub
		return entityManager.find(Contact.class,contactid);
	}

	@Override
	public boolean contactExists(String title, String first_name, String middle_name, String last_name) {
		// TODO Auto-generated method stub
		String ckeckExistQuery = QueryConstants.QUERY_CHECK_CONTACT_EXISTS;
		int count = entityManager.createNativeQuery(ckeckExistQuery).setParameter(1, title)
		              .setParameter(2, first_name).setParameter(3, middle_name).setParameter(4, last_name)
		              .getResultList().size();
		return count > 0 ? true : false;
	}
	
	@Override
	public List<Contact> searchContacts(String company_name, String company_website,
			String sector_of_operation,String designation,String department) {
		// TODO Auto-generated method stub
		List<Contact> list = new ArrayList<Contact>();

		String searchContactQuery = QueryConstants.QUERY_SEARCH_CONTACTS;
		List<Object[]> li =  (List<Object[]>) entityManager.createNativeQuery(searchContactQuery).setParameter(1, company_name)
	              .setParameter(2, company_website).setParameter(3, sector_of_operation).setParameter(4, designation)
	              .setParameter(5, department).getResultList();
		for(Object[] ob : li){
		    Contact contact = new Contact();
			for(int i=0;i<ob.length;i++) {
				if(i== 0) {
				  contact.setContact_id(Integer.parseInt(ob[i].toString()));
				}
				if(i== 1) {
					contact.setTitle(ob[1].toString());
					}
				if(i== 2) {
					contact.setFirst_name(ob[2].toString());
					}
				if(i== 3) {
					contact.setMiddle_name(ob[3].toString());
					}
				if(i== 4) {
					contact.setLast_name(ob[4].toString());
					}
				if(i== 5) {
					contact.setCompany_name(ob[5].toString());
					}
				if(i== 6) {
					contact.setRole(ob[6].toString());
					}
				if(i== 7) {
					contact.setDesignation(ob[7].toString());
					}
				if(i== 8) {
					contact.setDepartment(ob[8].toString());
					}
				if(i== 9) {
					contact.setSector_of_operation(ob[9].toString());
					}
				if(i== 10) {
					contact.setCompany_type(ob[10].toString());
					}
				if(i== 11) {
					contact.setOfficial_emailid(ob[11].toString());
					}
				if(i== 12) {
					contact.setPersonal_emailid(ob[12].toString());
					}
				if(i== 13) {
					contact.setContactno_mobile(ob[13].toString());
					}
				if(i== 14) {
					contact.setContactno_office(ob[14].toString());
					}
				if(i== 15) {
					contact.setContactno_home(ob[15].toString());
					}
				if(i== 16) {
					contact.setFax1(ob[16].toString());
					}
				if(i== 17) {
					contact.setFax2(ob[17].toString());
					}
				if(i== 18) {
					contact.setCompany_website(ob[18].toString());
					}
				if(i== 19) {
					contact.setKpmg_emailid(ob[19].toString());
					}
				if(i== 20) {
					contact.setLevel_of_relationship(ob[20].toString());
					}
				if(i== 21) {
					contact.setAddress(ob[21].toString());
					}
				if(i== 22) {
					contact.setPermanent_address(ob[22].toString());
					}
				if(i== 23) {
					contact.setCity(ob[23].toString());
					}
				if(i== 24) {
					contact.setZip(ob[24].toString());
					}
				if(i== 25) {
					contact.setCountry(ob[25].toString());
					}
				if(i== 26) {
					contact.setContact_info_last(ob[26].toString());					}
				}
		  list.add(contact);
		}
		return list;
	}

}
