package com.edds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

import com.edds.config.AuthenticationFilter;

@SpringBootApplication
public class EddsApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(EddsApplication.class, args);
	}
}
