package com.edds.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="edds_user")
public class LoginCredentials implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="userid")
    private String userid;
	
	@Column(name="user_fnm")
    private String user_fnm;
	
	@Column(name="user_mail")
    private String user_mail;
	
	@Column(name="user_phone")
    private String user_phone;
	
	@Column(name="pwd")
    private String password;
	
	@Column(name="active_from")
    private Date active_from;
	
	@Column(name="active_upto")
    private Date active_upto;

	@Column(name="loginid")
    private String loginid;  
	
	@Column(name="last_login")
    private String last_login;
	
	@Column(name="isactive")
    private boolean isactive;
	
	@Column(name="user_lnm")
    private String user_lnm;
	
	@Column(name="created_on")
    private Date created_on;
	
	@Column(name="created_by")
    private String created_by;
	
	@Column(name="updated_on")
    private Date updated_on;
	
	@Column(name="updated_by")
    private String updated_by;
	
	private String token;

	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getUser_fnm() {
		return user_fnm;
	}
	public void setUser_fnm(String user_fnm) {
		this.user_fnm = user_fnm;
	}
	public String getUser_mail() {
		return user_mail;
	}
	public void setUser_mail(String user_mail) {
		this.user_mail = user_mail;
	}
	public String getUser_phone() {
		return user_phone;
	}
	public void setUser_phone(String user_phone) {
		this.user_phone = user_phone;
	}
	public Date getActive_from() {
		return active_from;
	}
	public void setActive_from(Date active_from) {
		this.active_from = active_from;
	}
	public Date getActive_upto() {
		return active_upto;
	}
	public void setActive_upto(Date active_upto) {
		this.active_upto = active_upto;
	}
	public String getLast_login() {
		return last_login;
	}
	public void setLast_login(String last_login) {
		this.last_login = last_login;
	}
	public boolean isIsactive() {
		return isactive;
	}
	public void setIsactive(boolean isactive) {
		this.isactive = isactive;
	}
	public String getUser_lnm() {
		return user_lnm;
	}
	public void setUser_lnm(String user_lnm) {
		this.user_lnm = user_lnm;
	}
	public Date getCreated_on() {
		return created_on;
	}
	public void setCreated_on(Date created_on) {
		this.created_on = created_on;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	public Date getUpdated_on() {
		return updated_on;
	}
	public void setUpdated_on(Date updated_on) {
		this.updated_on = updated_on;
	}
	public String getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getLoginid() {
		return loginid;
	}
	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	@Override
	public String toString() {
		return "LoginCredentials [userid=" + userid + ", user_fnm=" + user_fnm + ", user_mail=" + user_mail
				+ ", user_phone=" + user_phone + ", password=" + password + ", active_from=" + active_from
				+ ", active_upto=" + active_upto + ", loginid=" + loginid + ", last_login=" + last_login + ", isactive="
				+ isactive + ", user_lnm=" + user_lnm + ", created_on=" + created_on + ", created_by=" + created_by
				+ ", updated_on=" + updated_on + ", updated_by=" + updated_by + "]";
	}
}
