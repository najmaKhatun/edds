package com.edds.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="user_token")
public class UserToken implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="userid")
    private int userid;  
	
	@Column(name="from_date")
	private Date from_date;
	
	@Column(name="to_date")
	private Date to_date;
	
	@Column(name="token")
	private String token;



	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public Date getFrom_date() {
		return from_date;
	}

	public void setFrom_date(Date from_date) {
		this.from_date = from_date;
	}

	public Date getTo_date() {
		return to_date;
	}

	public void setTo_date(Date to_date) {
		this.to_date = to_date;
	}

	@Override
	public String toString() {
		return "UserToken [userid=" + userid + ", from_date=" + from_date + ", to_date=" + to_date + ", token=" + token
				+ "]";
	}


	
}
