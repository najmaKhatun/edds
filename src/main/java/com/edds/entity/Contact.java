package com.edds.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="contact")
public class Contact implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="contact_id")
    private int contact_id;  
	
	@Column(name="title")
    private String title;
	
	@Column(name="first_name")
    private String first_name;
	
	@Column(name="middle_name")
    private String middle_name;
	
	@Column(name="last_name")
    private String last_name;
	
	@Column(name="company_name")
    private String company_name;
	
	@Column(name="role")
    private String role;
	
	@Column(name="designation")
    private String designation;
	
	@Column(name="department")
    private String department;
	
	@Column(name="sector_of_operation")
    private String sector_of_operation;
	
	@Column(name="company_type")
    private String company_type;
	
	@Column(name="official_emailid")
    private String official_emailid;
	
	@Column(name="personal_emailid")
    private String personal_emailid;
	
	@Column(name="contactno_mobile")
    private String contactno_mobile;
	
	@Column(name="contactno_office")
    private String contactno_office;
	
	@Column(name="contactno_home")
    private String contactno_home;
	
	@Column(name="fax1")
    private String fax1;
	
	@Column(name="fax2")
    private String fax2;
	
	@Column(name="company_website")
    private String company_website;
	
	@Column(name="kpmg_emailid")
    private String kpmg_emailid;
	
	@Column(name="level_of_relationship")
    private String level_of_relationship;
	
	@Column(name="address")
    private String address;
	
	@Column(name="permanent_address")
    private String permanent_address;
	
	@Column(name="city")
    private String city;
	
	@Column(name="zip")
    private String zip;
	
	@Column(name="country")
    private String country;
	
	@Column(name="contact_info_last")
    private String contact_info_last;

	public int getContact_id() {
		return contact_id;
	}

	public void setContact_id(int contact_id) {
		this.contact_id = contact_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getMiddle_name() {
		return middle_name;
	}

	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getCompany_name() {
		return company_name;
	}

	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getSector_of_operation() {
		return sector_of_operation;
	}

	public void setSector_of_operation(String sector_of_operation) {
		this.sector_of_operation = sector_of_operation;
	}

	public String getCompany_type() {
		return company_type;
	}

	public void setCompany_type(String company_type) {
		this.company_type = company_type;
	}

	public String getOfficial_emailid() {
		return official_emailid;
	}

	public void setOfficial_emailid(String official_emailid) {
		this.official_emailid = official_emailid;
	}

	public String getPersonal_emailid() {
		return personal_emailid;
	}

	public void setPersonal_emailid(String personal_emailid) {
		this.personal_emailid = personal_emailid;
	}

	public String getContactno_mobile() {
		return contactno_mobile;
	}

	public void setContactno_mobile(String contactno_mobile) {
		this.contactno_mobile = contactno_mobile;
	}

	public String getContactno_office() {
		return contactno_office;
	}

	public void setContactno_office(String contactno_office) {
		this.contactno_office = contactno_office;
	}

	public String getContactno_home() {
		return contactno_home;
	}

	public void setContactno_home(String contactno_home) {
		this.contactno_home = contactno_home;
	}

	public String getFax1() {
		return fax1;
	}

	public void setFax1(String fax1) {
		this.fax1 = fax1;
	}

	public String getFax2() {
		return fax2;
	}

	public void setFax2(String fax2) {
		this.fax2 = fax2;
	}

	public String getCompany_website() {
		return company_website;
	}

	public void setCompany_website(String company_website) {
		this.company_website = company_website;
	}

	public String getKpmg_emailid() {
		return kpmg_emailid;
	}

	public void setKpmg_emailid(String kpmg_emailid) {
		this.kpmg_emailid = kpmg_emailid;
	}

	public String getLevel_of_relationship() {
		return level_of_relationship;
	}

	public void setLevel_of_relationship(String level_of_relationship) {
		this.level_of_relationship = level_of_relationship;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPermanent_address() {
		return permanent_address;
	}

	public void setPermanent_address(String permanent_address) {
		this.permanent_address = permanent_address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getContact_info_last() {
		return contact_info_last;
	}

	public void setContact_info_last(String contact_info_last) {
		this.contact_info_last = contact_info_last;
	}

	@Override
	public String toString() {
		return "Contact [contact_id=" + contact_id + ", title=" + title + ", first_name=" + first_name
				+ ", middle_name=" + middle_name + ", last_name=" + last_name + ", company_name=" + company_name
				+ ", role=" + role + ", designation=" + designation + ", department=" + department
				+ ", sector_of_operation=" + sector_of_operation + ", company_type=" + company_type
				+ ", official_emailid=" + official_emailid + ", personal_emailid=" + personal_emailid
				+ ", contactno_mobile=" + contactno_mobile + ", contactno_office=" + contactno_office
				+ ", contactno_home=" + contactno_home + ", fax1=" + fax1 + ", fax2=" + fax2 + ", company_website="
				+ company_website + ", kpmg_emailid=" + kpmg_emailid + ", level_of_relationship="
				+ level_of_relationship + ", address=" + address + ", permanent_address=" + permanent_address
				+ ", city=" + city + ", zip=" + zip + ", country=" + country + ", contact_info_last="
				+ contact_info_last + "]";
	}
	
	
}
