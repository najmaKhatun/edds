package com.edds.entity;

public class MyAPIResponseTO<T> {
	String message;
	int status;
	private T data;
	
	public MyAPIResponseTO() {
		// Constructor without argument.
	}
	
	public MyAPIResponseTO(String message, int status, T data) {
		super();
		this.message = message;
		this.status = status;
		this.data = data;
	}
	
	public MyAPIResponseTO(String message, int i) {
		this.message=message;
		this.status=i;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
	
	
	

}
