package com.edds.config;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.edds.dao.AuthenticationDAO;
import com.edds.exception.MyException;
@Component
public class AuthenticationFilter  extends OncePerRequestFilter{
	@Autowired
	AuthenticationDAO authDao;
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
//		System.out.println("If is =>"+(request.getRequestURI().matches("/api/eddsV1/login")
//			|| 	request.getRequestURI().matches("/v2/api-docs")
//			|| 	request.getRequestURI().matches("/swagger-ui.html")
//			|| 	request.getRequestURI().matches("/favicon.ico")
//			)+ " request =>"+request.getRequestURI().toString());
		
		String authorizationHeader = request.getHeader("Authorization");
		if(!(request.getRequestURI().matches("/api/eddsV1/login")
			|| request.getRequestURI().matches("/v2/api-docs")
			|| request.getRequestURI().matches("/swagger(.*)")
			|| request.getRequestURI().matches("/favicon.ico")
			|| request.getRequestURI().matches("/webjars(.*)")
			|| request.getRequestURI().matches("/csrf")
			)) {
			
			
			if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
				try {
					throw new MyException("Authorization header must be provided");
				} catch (MyException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			String token = authorizationHeader.substring("Bearer".length()).trim();
			if(authDao.checkTokenValidity(token)) {
				filterChain.doFilter(request, response);
			}else {
				try {
					throw new MyException("Unauthorized");
				} catch (MyException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		else {
			filterChain.doFilter(request, response);
		}
	  }	
	}

